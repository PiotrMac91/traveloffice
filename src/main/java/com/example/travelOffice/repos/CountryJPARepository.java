package com.example.travelOffice.repos;

import com.example.travelOffice.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryJPARepository extends JpaRepository<Country, Long> {
}
