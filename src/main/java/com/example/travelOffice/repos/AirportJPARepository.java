package com.example.travelOffice.repos;

import com.example.travelOffice.models.Airport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AirportJPARepository extends JpaRepository<Airport, Long> {
}
