package com.example.travelOffice.repos;

import com.example.travelOffice.models.Role;
import com.example.travelOffice.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface UserJPARepository extends JpaRepository<User, Integer> {
    Optional<User> findByName(String username);

    Optional<User> findById (Integer id);
    
}
