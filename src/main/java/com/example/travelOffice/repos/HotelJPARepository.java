package com.example.travelOffice.repos;

import com.example.travelOffice.models.City;
import com.example.travelOffice.models.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HotelJPARepository extends JpaRepository<Hotel, Long> {
//CI_ID, CI_NAME
    @Query(value = "SELECT * FROM city WHERE CI_ID = :parameter", nativeQuery = true)
    City findCityDetailsById(@Param("parameter") Long parameter);

}
