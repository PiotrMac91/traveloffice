package com.example.travelOffice.repos;

import com.example.travelOffice.models.City;
import com.example.travelOffice.models.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TripJPARepository extends JpaRepository<Trip, Long> {

    @Query(value = "SELECT * FROM trip WHERE tr_promoted = true", nativeQuery = true)
    List<Trip> findPromotedTrip();

    @Query(value = "select * from trip where trip.tr_departure_date <  DATE_Add(NOW(), INTERVAL 30 DAY) and trip.tr_departure_date > NOW()", nativeQuery = true)
    List<Trip> findComingTrip();

//    @Query(value = "SELECT * FROM trip JOIN city on trip.tr_ci_destination = city.ci_id JOIN country ON country.cu_id = city.ci_cu_id JOIN continent on continent.co_id = country.cu_co_id WHERE continent.co_name = :param ", nativeQuery = true)
//    List<Trip> findContinentTrip(@Param("continent")String param);
}
