package com.example.travelOffice.repos;

import com.example.travelOffice.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleJPARepository extends JpaRepository<Role, Integer> {
    Role findRoleById (Integer id);
//
////    Optional<Role> findByRole(String role);
}
