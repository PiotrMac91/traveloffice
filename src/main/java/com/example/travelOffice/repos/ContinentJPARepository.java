package com.example.travelOffice.repos;

import com.example.travelOffice.models.Continent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContinentJPARepository extends JpaRepository<Continent, Long> {
}
