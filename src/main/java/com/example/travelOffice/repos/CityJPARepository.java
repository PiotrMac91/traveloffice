package com.example.travelOffice.repos;

import com.example.travelOffice.models.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CityJPARepository extends JpaRepository<City, Long> {

    Optional<City> findById(Long value);
}
