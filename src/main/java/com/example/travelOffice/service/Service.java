package com.example.travelOffice.service;

import com.example.travelOffice.models.*;
import com.example.travelOffice.repos.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class Service {

    @Autowired
    private AirportJPARepository airportJPARepository;

    @Autowired
    private CityJPARepository cityJPARepository;

    @Autowired
    private ContinentJPARepository continentJPARepository;

    @Autowired
    private CountryJPARepository countryJPARepository;

    @Autowired
    private HotelJPARepository hotelJPARepository;

    @Autowired
    private TripJPARepository tripJPARepository;

    @Autowired
    private UserJPARepository userJPARepository;

    public List<Continent> getContinentList() {
        List<Continent> continents = continentJPARepository.findAll();
        return continents;
    }

    public List<Country> getCountryList() {
        List<Country> countryList = countryJPARepository.findAll();
        return countryList;
    }

    public List<City> getCityList() {
        List<City> cityList = cityJPARepository.findAll();
        return cityList;
    }

    public List<Airport> getAirportList() {
        List<Airport> airportList = airportJPARepository.findAll();
        return airportList;
    }

    public List<Hotel> getHotelList() {
        List<Hotel> hotelList = hotelJPARepository.findAll();
        return hotelList;
    }

    public Optional<City> getCityDetailsByHotelCityId(Long city) {
        Optional<City> cityObj = cityJPARepository.findById(city);
        return cityObj;
    }

    public List<Trip> getTripList() {
        List<Trip> tripList = tripJPARepository.findAll();
        return tripList;
    }

    public List<Trip> getPromotedTrip() {
        List<Trip> tripList = tripJPARepository.findPromotedTrip();
        return tripList;
    }

    public List<Trip> getComingTrip() {
        List<Trip> tripList = tripJPARepository.findComingTrip();
        return tripList;
    }

    public boolean findHotelById(String id) {
        Optional<Hotel> hotels = hotelJPARepository.findById(Long.valueOf(id));
        return true;
    }

    public List<User> getUserList() {
        List<User> users = userJPARepository.findAll();
        return users;
    }

//    public List<Trip> getContinentTrip(Continent continent){
//        List<Trip> tripList = tripJPARepository.findContinentTrip(continent.getName());
//        return tripList;
//    }

//    public City findCityById()
//        return

//    public CityJPARepository getCityJPARepository(Long value) {
//        return cityJPARepository.getCityByID(value);

}
