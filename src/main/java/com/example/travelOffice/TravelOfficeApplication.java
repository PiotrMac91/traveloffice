package com.example.travelOffice;

import com.example.travelOffice.service.Service;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TravelOfficeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelOfficeApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public Service service() {return new Service();}


}
