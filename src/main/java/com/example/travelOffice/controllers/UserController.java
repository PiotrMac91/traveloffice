package com.example.travelOffice.controllers;

import com.example.travelOffice.dtos.CityDTO;
import com.example.travelOffice.dtos.UserDTO;
import com.example.travelOffice.models.City;
import com.example.travelOffice.models.User;
import com.example.travelOffice.service.Service;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@PreAuthorize("permitAll()")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Service service;

    @GetMapping
    @ResponseBody
    List<UserDTO> getUserList(){
        List<User> users = service.getUserList();
        return users.stream().map(user -> convertToDto(user)).collect(Collectors.toList());
    }

    private UserDTO convertToDto(User user) {
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return userDTO;
    }
}
