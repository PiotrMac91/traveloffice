package com.example.travelOffice.controllers;

import com.example.travelOffice.dtos.ContinentDTO;
import com.example.travelOffice.models.Continent;
import com.example.travelOffice.repos.ContinentJPARepository;
import com.example.travelOffice.service.Service;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/continent")
public class ContinentController {
    private ContinentJPARepository continentJPARepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Service service;

    public ContinentController(ContinentJPARepository continentJPARepository) {
        this.continentJPARepository = continentJPARepository;
    }

    @GetMapping
    @ResponseBody
    List<ContinentDTO> getContinentList() {
        List<Continent> continents = service.getContinentList();
        return continents.stream().map(continent -> convertToDto(continent)).collect(Collectors.toList());
    }

    private ContinentDTO convertToDto(Continent continent) {
        ContinentDTO continentDTO = modelMapper.map(continent, ContinentDTO.class);
        //continentDTO.setName(continent.getName());
        return continentDTO;
    }

    private Continent convertToEntity(ContinentDTO continentDTO) {
        Continent continent = modelMapper.map(continentDTO, Continent.class);
        continent.setName(continentDTO.getName());
        return continent;
    }

}
