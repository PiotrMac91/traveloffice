package com.example.travelOffice.controllers;

import com.example.travelOffice.dtos.HotelDTO;
import com.example.travelOffice.models.City;
import com.example.travelOffice.repos.HotelJPARepository;
import com.example.travelOffice.models.Hotel;
import com.example.travelOffice.service.Service;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.Long.parseLong;

@Controller
@RequestMapping("/hotel")
public class HotelController {

    private HotelJPARepository hotelJPARepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Service service;

    public HotelController(HotelJPARepository hotelJPARepository) {
        this.hotelJPARepository = hotelJPARepository;
    }

    @GetMapping
    @ResponseBody
    List<HotelDTO> getHotelList() {
        List<Hotel> hotels = service.getHotelList();
        return hotels.stream().map(hotel -> convertToDto(hotel)).collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public HotelDTO createHotel(@RequestBody HotelDTO hotelDTO) {
        Hotel hotel = convertToEntity(hotelDTO);
        Hotel hotelCreated = hotelJPARepository.save(hotel);
        return convertToDto(hotelCreated);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void putHotel(@RequestBody HotelDTO hotelDTO, @PathVariable String id) {
        Optional<City> findCityDetails = service.getCityDetailsByHotelCityId(parseLong(hotelDTO.getCity()));
        Hotel hotel = new Hotel();

        if (service.findHotelById(id) == true) {
            hotel.setId(Long.valueOf(id));
            hotel.setStandard(hotelDTO.getStandard());
            hotel.setDescription(hotelDTO.getDescription());
            hotel.setName(hotelDTO.getName());
            hotel.setCity(findCityDetails.orElseThrow());
            hotelJPARepository.save(hotel);
        } else {
            hotel.setStandard(hotelDTO.getStandard());
            hotel.setDescription(hotelDTO.getDescription());
            hotel.setName(hotelDTO.getName());
            hotel.setCity(findCityDetails.orElseThrow());
            hotelJPARepository.save(hotel);
        }
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    void deleteHotel(@PathVariable("id") Hotel hotel) {
        hotelJPARepository.deleteById(hotel.getId());
    }

    private HotelDTO convertToDto(Hotel hotel) {
        HotelDTO hotelDTO = modelMapper.map(hotel, HotelDTO.class);
        hotelDTO.setCity(hotel.getCity().getName());
        return hotelDTO;
    }

    private Hotel convertToEntity(HotelDTO hotelDTO) {

        Optional<City> findCityDetails = service.getCityDetailsByHotelCityId(parseLong(hotelDTO.getCity()));
        Hotel hotel = new Hotel(); //modelMapper.map(hotelDTO, Hotel.class);    // Tu sie wywala
        hotel.setStandard(hotelDTO.getStandard());
        hotel.setDescription(hotelDTO.getDescription());
        hotel.setName(hotelDTO.getName());
        hotel.setCity(findCityDetails.orElseThrow());
        return hotel;
    }
}
