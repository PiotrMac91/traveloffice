package com.example.travelOffice.controllers;

import com.example.travelOffice.dtos.CityDTO;
import com.example.travelOffice.repos.CityJPARepository;
import com.example.travelOffice.models.City;
import com.example.travelOffice.service.Service;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/city")
public class CityController {

    private CityJPARepository cityJPARepository;

    public CityController(CityJPARepository cityJPARepository) {
        this.cityJPARepository = cityJPARepository;
    }

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Service service;


    @GetMapping
    @ResponseBody
    List<CityDTO> getCityList(){
        List<City> cities = service.getCityList();
        return cities.stream().map(city -> convertToDto(city)).collect(Collectors.toList());
    }

    private CityDTO convertToDto(City city) {
        CityDTO cityDTO = modelMapper.map(city, CityDTO.class);
        cityDTO.setCountry(city.getCountryId().getName());
        return cityDTO;
    }

    private City convertToEntity(CityDTO cityDTO) {
        City city = modelMapper.map(cityDTO, City.class);
        city.setName(cityDTO.getName());
        return city;
    }

}