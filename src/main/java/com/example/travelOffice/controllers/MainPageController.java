package com.example.travelOffice.controllers;

import com.example.travelOffice.dtos.TripDTO;
import com.example.travelOffice.models.Trip;
import com.example.travelOffice.repos.TripJPARepository;
import com.example.travelOffice.service.Service;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.stream.Collectors;

@Controller
@RequestMapping({"/home", "/"})
public class MainPageController {
    private TripJPARepository tripJPARepository;

    public MainPageController(TripJPARepository tripJPARepository) {
        this.tripJPARepository = tripJPARepository;
    }

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Service service;

//    @GetMapping
//    @RequestMapping("/home")
//    public String displayIndex(){
//        return "index";
//    }

    @GetMapping
    public ModelAndView getPromotedTrip(){
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("promotes",service.getPromotedTrip().stream().map(trip -> convertToDto(trip)).collect(Collectors.toList()));
        return mav;
    }


    private TripDTO convertToDto(Trip trip) {
        TripDTO tripDTO = modelMapper.map(trip, TripDTO.class);
        tripDTO.setCityOrigin(trip.getCityOrigin().getName());
        tripDTO.setCityDestination(trip.getCityDestination().getName());
        return tripDTO;
    }

    private Trip convertToEntity(TripDTO tripDTO) {
        Trip trip = modelMapper.map(tripDTO, Trip.class);
        return trip;
    }

}
