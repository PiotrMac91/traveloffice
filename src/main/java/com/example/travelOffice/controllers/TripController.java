package com.example.travelOffice.controllers;

import com.example.travelOffice.dtos.CountryDTO;
import com.example.travelOffice.dtos.TripDTO;
import com.example.travelOffice.models.Country;
import com.example.travelOffice.models.Trip;
import com.example.travelOffice.repos.TripJPARepository;
import com.example.travelOffice.service.Service;
import org.dom4j.rule.Mode;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/trip")
public class TripController {
    private TripJPARepository tripJPARepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Service service;

    public TripController(TripJPARepository tripJPARepository) {
        this.tripJPARepository = tripJPARepository;
    }

    @GetMapping
    @ResponseBody
    List<TripDTO> getTripList(){
        List<Trip> trips = service.getTripList();
        return trips.stream().map(trip -> convertToDto(trip)).collect(Collectors.toList());
    }

    @GetMapping("/promoted")
    @ResponseBody
    List<TripDTO> getPromotedTrip(){
        List<Trip> trips = service.getPromotedTrip();
        return trips.stream().map(trip -> convertToDto(trip)).collect(Collectors.toList());
    }

//    @GetMapping("/promoted")
//    public ModelAndView getPromotedTrip(){
//        ModelAndView mav = new ModelAndView("promotes");
//        mav.addObject("promotes",service.getPromotedTrip().stream().map(trip -> convertToDto(trip)).collect(Collectors.toList()));
//        return mav;
//    }

    @GetMapping("/coming")
    @ResponseBody
    List<TripDTO> getComingTrip(){
        List<Trip> trips = service.getComingTrip();
        return trips.stream().map(trip -> convertToDto(trip)).collect(Collectors.toList());
    }


    private TripDTO convertToDto(Trip trip) {
        TripDTO tripDTO = modelMapper.map(trip, TripDTO.class);
        tripDTO.setCityOrigin(trip.getCityOrigin().getName());
        tripDTO.setCityDestination(trip.getCityDestination().getName());
        return tripDTO;
    }

    private Trip convertToEntity(TripDTO tripDTO) {
        Trip trip = modelMapper.map(tripDTO, Trip.class);
        return trip;
    }

}
