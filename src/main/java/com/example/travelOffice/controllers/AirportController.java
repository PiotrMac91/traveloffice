package com.example.travelOffice.controllers;

import com.example.travelOffice.dtos.AirportDTO;
import com.example.travelOffice.models.Airport;
import com.example.travelOffice.repos.AirportJPARepository;
import com.example.travelOffice.service.Service;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/airport")
public class AirportController {

    private AirportJPARepository airportJPARepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Service service;


    public AirportController(AirportJPARepository airportJPARepository) {
        this.airportJPARepository = airportJPARepository;
    }

    @GetMapping
    @ResponseBody
    List<AirportDTO> getAirportList(){
        List<Airport> airports = service.getAirportList();
        return airports.stream().map(airport -> convertToDto(airport)).collect(Collectors.toList());
    }


    private AirportDTO convertToDto(Airport airport) {
        AirportDTO airportDTO = modelMapper.map(airport, AirportDTO.class);
        airportDTO.setCity(airport.getCity_id().getName());
        return airportDTO;
    }

    private Airport convertToEntity(AirportDTO airportDTO) {
        Airport airport = modelMapper.map(airportDTO, Airport.class);
        airport.setName(airportDTO.getName());
        return airport;
    }
}
