package com.example.travelOffice.controllers;

import com.example.travelOffice.dtos.HotelDTO;
import com.example.travelOffice.dtos.TripDTO;
import com.example.travelOffice.dtos.UserDTO;
import com.example.travelOffice.models.Hotel;
import com.example.travelOffice.models.Role;
import com.example.travelOffice.models.Trip;
import com.example.travelOffice.models.User;
import com.example.travelOffice.repos.RoleJPARepository;
import com.example.travelOffice.repos.UserJPARepository;
import com.example.travelOffice.service.Service;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@PreAuthorize("permitAll()")
@RequestMapping("/registration")
public class RegistrationController {
    //    private static final String[] DEFAULT_ROLES = new String[] {"USER"};
//    private static final Integer[] DEFAULT_ROLES = new Integer[] {2};
    private static final Integer DEFAULT_ROLE = 2;
    private static final BCrypt hashing = new BCrypt();

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserJPARepository userJPARepository;

    @Autowired
    private RoleJPARepository roleJPARepository;

    public String hash(String password){
        return BCrypt.hashpw(password,BCrypt.gensalt(10));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void registerUser(@RequestBody UserDTO userDTO) {
        User user = convertToEntity(userDTO);
        user.addRole(getRole(DEFAULT_ROLE));
        user.setPassword(hash(user.getPassword()));
        userJPARepository.save(user);
    }


    private UserDTO convertToDto(User user) {
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return userDTO;
    }

    private User convertToEntity(UserDTO userDTO) {
        User user = modelMapper.map(userDTO, User.class);
        user.setActive(1);
        return user;
    }


    private Role getRole(Integer role) {
        return roleJPARepository.findRoleById(role);
    }

}
