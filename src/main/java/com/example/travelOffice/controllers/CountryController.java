package com.example.travelOffice.controllers;

import com.example.travelOffice.dtos.CountryDTO;
import com.example.travelOffice.models.Country;
import com.example.travelOffice.repos.CountryJPARepository;
import com.example.travelOffice.service.Service;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/country")
public class CountryController {
    private CountryJPARepository countryJPARepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Service service;

    public CountryController(CountryJPARepository countryJPARepository) {
        this.countryJPARepository = countryJPARepository;
    }

    @GetMapping
    @ResponseBody
    List<CountryDTO> getCountryList() {
        List<Country> countries = service.getCountryList();
        return countries.stream().map(country -> convertToDto(country)).collect(Collectors.toList());
    }

    private CountryDTO convertToDto(Country country) {
        CountryDTO countryDTO = modelMapper.map(country, CountryDTO.class);
        countryDTO.setContinent(country.getContinentId().getName());
        return countryDTO;
    }

    private Country convertToEntity(CountryDTO countryDTO) {
        Country country = modelMapper.map(countryDTO, Country.class);
        return country;
    }

}
