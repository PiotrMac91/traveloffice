package com.example.travelOffice.dtos;

import com.example.travelOffice.models.TripType;

import java.time.LocalDate;

public class TripDTO {

    private String cityOrigin;
    private String cityDestination;
    private LocalDate departureDate;
    private LocalDate returnDate;
    private int tripDaysCount;
    private TripType tripType;
    private int adultPrice;
    private int childPrice;
    private Boolean promoted;
    private int tripAdultCount;
    private int tripChildCount;


    public TripDTO() {
    }

    public TripDTO(String cityOrigin, String cityDestination, LocalDate departureDate, LocalDate returnDate, int tripDaysCount, TripType tripType, int adultPrice, int childPrice, Boolean promoted, int tripAdultCount, int tripChildCount) {
        this.cityOrigin = cityOrigin;
        this.cityDestination = cityDestination;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.tripDaysCount = tripDaysCount;
        this.tripType = tripType;
        this.adultPrice = adultPrice;
        this.childPrice = childPrice;
        this.promoted = promoted;
        this.tripAdultCount = tripAdultCount;
        this.tripChildCount = tripChildCount;
    }

    public String getCityOrigin() {
        return cityOrigin;
    }

    public void setCityOrigin(String cityOrigin) {
        this.cityOrigin = cityOrigin;
    }

    public String getCityDestination() {
        return cityDestination;
    }

    public void setCityDestination(String cityDestination) {
        this.cityDestination = cityDestination;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public int getTripDaysCount() {
        return tripDaysCount;
    }

    public void setTripDaysCount(int tripDaysCount) {
        this.tripDaysCount = tripDaysCount;
    }

    public TripType getTripType() {
        return tripType;
    }

    public void setTripType(TripType tripType) {
        this.tripType = tripType;
    }

    public int getAdultPrice() {
        return adultPrice;
    }

    public void setAdultPrice(int adultPrice) {
        this.adultPrice = adultPrice;
    }

    public int getChildPrice() {
        return childPrice;
    }

    public void setChildPrice(int childPrice) {
        this.childPrice = childPrice;
    }

    public Boolean getPromoted() {
        return promoted;
    }

    public void setPromoted(Boolean promoted) {
        this.promoted = promoted;
    }

    public int getTripAdultCount() {
        return tripAdultCount;
    }

    public void setTripAdultCount(int tripAdultCount) {
        this.tripAdultCount = tripAdultCount;
    }

    public int getTripChildCount() {
        return tripChildCount;
    }

    public void setTripChildCount(int tripChildCount) {
        this.tripChildCount = tripChildCount;
    }
}
