package com.example.travelOffice.dtos;

import com.example.travelOffice.models.HotelStandard;

public class HotelDTO {

    private Long id;
    private String name;
    private HotelStandard standard;
    private String description;
    private String city;

    public HotelDTO() {
    }

    public HotelDTO(Long id, String name, HotelStandard standard, String description, String city) {
        this.id = id;
        this.name = name;
        this.standard = standard;
        this.description = description;
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HotelStandard getStandard() {
        return standard;
    }

    public void setStandard(HotelStandard standard) {
        this.standard = standard;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
