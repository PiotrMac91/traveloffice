package com.example.travelOffice.dtos;

import com.example.travelOffice.models.Continent;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;


public class ContinentDTO {

    private Long Id;
    private String name;

    public ContinentDTO() {
    }

    public ContinentDTO(Long id, String name) {
        Id = id;
        this.name = name;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
