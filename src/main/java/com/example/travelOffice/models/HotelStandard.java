package com.example.travelOffice.models;

public enum HotelStandard {
    ONE_STAR,
    TWO_STAR,
    THREE_STAR,
    FOUR_STAR,
    FIVE_STAR;
}

