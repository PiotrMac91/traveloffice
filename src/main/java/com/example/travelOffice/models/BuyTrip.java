package com.example.travelOffice.models;

import com.example.travelOffice.models.Trip;

import javax.persistence.*;

@Entity
@Table
public class BuyTrip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BT_ID", nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "BT_TR_ID", nullable = false)
    private Trip tripId;

    @Column(name = "BT_USER", nullable = false)
    private String userId;

    @Column(name = "BT_PRICE", nullable = false)
    private int price;

    public BuyTrip() {
    }

    public BuyTrip(Trip tripId, String userId, int price) {
        this.tripId = tripId;
        this.userId = userId;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Trip getTripId() {
        return tripId;
    }

    public void setTripId(Trip tripId) {
        this.tripId = tripId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BuyTrip{" +
                "id=" + id +
                ", tripId=" + tripId +
                ", userId='" + userId + '\'' +
                ", price=" + price +
                '}';
    }
}
