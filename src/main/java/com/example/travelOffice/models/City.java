package com.example.travelOffice.models;

import com.example.travelOffice.models.Country;

import javax.persistence.*;

@Entity
@Table
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CI_ID", nullable = false)
    private Long id;

    @Column(name = "CI_NAME", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "CI_CU_ID", nullable = false)
    private Country countryId;

    public City() {
    }

    public City(String name, Country countryId) {
        this.name = name;
        this.countryId = countryId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountryId() {
        return countryId;
    }

    public void setCountryId(Country countryId) {
        this.countryId = countryId;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", countryId=" + countryId +
                '}';
    }
}
