package com.example.travelOffice.models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TR_ID", nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "TR_CI_ORIGIN", nullable = false)
    private City cityOrigin;

    @OneToOne
    @JoinColumn(name = "TR_CI_DESTINATION", nullable = false)
    private City cityDestination;

    @Column(name = "TR_DEPARTURE_DATE", nullable = false)
    private LocalDate departureDate;
    @Column(name = "TR_RETURN_DATE", nullable = false)
    private LocalDate returnDate;
    @Column(name = "TR_DAYS_COUNT", nullable = false)
    private int tripDaysCount;
    @Column(name = "TR_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private TripType tripType;
    @Column(name = "TR_ADULT_PRICE", nullable = false)
    private int adultPrice;
    @Column(name = "TR_CHILD_PRICE", nullable = false)
    private int childPrice;
    @Column(name = "TR_PROMOTED", nullable = false)
    private Boolean promoted;
    @Column(name = "TR_ADULT_COUNT", nullable = false)
    private int tripAdultCount;
    @Column(name = "TR_CHILD_COUNT", nullable = false)
    private int tripChildCount;

    public Trip() {
    }

    public Trip(City cityOrigin, City cityDestination, LocalDate departureDate, LocalDate returnDate, int tripDaysCount, TripType tripType, int adultPrice, int childPrice, Boolean promoted, int tripAdultCount, int tripChildCount) {
        this.cityOrigin = cityOrigin;
        this.cityDestination = cityDestination;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.tripDaysCount = tripDaysCount;
        this.tripType = tripType;
        this.adultPrice = adultPrice;
        this.childPrice = childPrice;
        this.promoted = promoted;
        this.tripAdultCount = tripAdultCount;
        this.tripChildCount = tripChildCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public City getCityOrigin() {
        return cityOrigin;
    }

    public void setCityOrigin(City cityOrigin) {
        this.cityOrigin = cityOrigin;
    }

    public City getCityDestination() {
        return cityDestination;
    }

    public void setCityDestination(City cityDestination) {
        this.cityDestination = cityDestination;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public int getTripDaysCount() {
        return tripDaysCount;
    }

    public void setTripDaysCount(int tripDaysCount) {
        this.tripDaysCount = tripDaysCount;
    }

    public TripType getTripType() {
        return tripType;
    }

    public void setTripType(TripType tripType) {
        this.tripType = tripType;
    }

    public int getAdultPrice() {
        return adultPrice;
    }

    public void setAdultPrice(int adultPrice) {
        this.adultPrice = adultPrice;
    }

    public int getChildPrice() {
        return childPrice;
    }

    public void setChildPrice(int childPrice) {
        this.childPrice = childPrice;
    }

    public Boolean getPromoted() {
        return promoted;
    }

    public void setPromoted(Boolean promoted) {
        this.promoted = promoted;
    }

    public int getTripAdultCount() {
        return tripAdultCount;
    }

    public void setTripAdultCount(int tripAdultCount) {
        this.tripAdultCount = tripAdultCount;
    }

    public int getTripChildCount() {
        return tripChildCount;
    }

    public void setTripChildCount(int tripChildCount) {
        this.tripChildCount = tripChildCount;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", cityOrigin=" + cityOrigin +
                ", cityDestination=" + cityDestination +
                ", departureDate=" + departureDate +
                ", returnDate=" + returnDate +
                ", tripDaysCount=" + tripDaysCount +
                ", tripType=" + tripType +
                ", adultPrice=" + adultPrice +
                ", childPrice=" + childPrice +
                ", promoted=" + promoted +
                ", tripAdultCount=" + tripAdultCount +
                ", tripChildCount=" + tripChildCount +
                '}';
    }
}
