package com.example.travelOffice.models;

import javax.persistence.*;

@Entity
@Table
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "HO_ID", nullable = false)
    private Long id;

    @Column(name = "HO_NAME", nullable = false)
    private String name;


    @Column(name = "HO_STANDARD", nullable = false)
    @Enumerated(EnumType.STRING)
    private HotelStandard standard;

    @Column(name = "HO_DESCRIPTION", nullable = false)
    private String description;

    @ManyToOne(targetEntity = City.class)
    @JoinColumn(name = "HO_CI_ID")
    private City city;


    public Hotel() {
    }

    public Hotel(String name, HotelStandard standard, String description, City city) {
        this.name = name;
        this.standard = standard;
        this.description = description;
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HotelStandard getStandard() {
        return standard;
    }

    public void setStandard(HotelStandard standard) {
        this.standard = standard;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", standard=" + standard +
                ", description='" + description + '\'' +
                ", city=" + city +
                '}';
    }
}
