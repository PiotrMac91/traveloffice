package com.example.travelOffice.models;

import com.example.travelOffice.models.City;

import javax.persistence.*;

@Entity
@Table
public class Airport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AI_ID", nullable = false)
    private Long id;

    @Column(name = "AI_NAME", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "AI_CI_ID", nullable = false)
    private City city_id;

    public Airport() {
    }

    public Airport(String name, City city_id) {
        this.name = name;
        this.city_id = city_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity_id() {
        return city_id;
    }

    public void setCity_id(City city_id) {
        this.city_id = city_id;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", city_id=" + city_id +
                '}';
    }
}
