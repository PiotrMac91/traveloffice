package com.example.travelOffice.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class UserRoleId implements Serializable {

    @Column(name = "user_id")
    private int userId;

    @Column(name = "role_id")
    private int roleId;

    public UserRoleId(int userId, int roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

    public UserRoleId() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }


    @Override
    public int hashCode() {
        return Objects.hash(userId,roleId);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;

        if(obj==null || getClass() != obj.getClass()) return false;

        UserRoleId that = (UserRoleId) obj;
        return Objects.equals(userId, that.userId) && Objects.equals(roleId, that.roleId);
    }
}
