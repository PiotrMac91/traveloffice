package com.example.travelOffice.models;

import com.example.travelOffice.dtos.ContinentDTO;

import javax.persistence.*;

@Entity
@Table
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CU_ID", nullable = false)
    private Long id;

    @Column(name = "CU_NAME", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "CU_CO_ID", nullable = false)
    private Continent continentId;

    public Country() {
    }

    public Country(String name, Continent continentId) {
        this.name = name;
        this.continentId = continentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Continent getContinentId() {
        return continentId;
    }

    public void setContinentId(Continent continentId) {
        this.continentId = continentId;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", continentId=" + continentId +
                '}';
    }
}
