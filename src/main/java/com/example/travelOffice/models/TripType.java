package com.example.travelOffice.models;

public enum TripType {
    BB,
    HB,
    FB,
    AI;
}
